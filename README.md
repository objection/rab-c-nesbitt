# Rab C. Nesbitt

This is a string library with various useful functions.

Generally they operate on arbitrary objects. You pass your array of
objects and a struct rab_str_getter with a "str-getting" function, ie,
one that returns the string member you're interested in. Because of
this, you can't say this library's functions are fast. But you
wouldn't be using library code if you were writing stuff you needed to
be fast.

## Install

You need Meson, which you can get in your package manager. Else, look
here:
<https://mesonbuild.com/Getting-meson.html>.

To install globally, do:

```shell
git clone https://objection/rab-c-nesbitt.git
cd rab-c-nesbitt
meson build
ninja -Cbuild install

```

That'll install the lib to (probably) /usr/local/lib64/.

If you just want to use the files, you can include it as a submodule
in you project (eg,

```shell
mkdir lib
git submodule add https://objection/rab-c-nesbitt.git lib/rab-c-nesbitt

```

).

You can also, of course, just use the files. There's just two of them.
